package util;



import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class JDBCConnection {
	
	private static Connection conn = null;
	
	public static Connection getConnection() {
		
		try {
			
			if(conn == null) {
				
				//Oracle added a *hotfix* to ensure that
				//the ojdbc drivers would correctly load
				//at the beginning of your application starting
				Class.forName("oracle.jdbc.driver.OracleDriver");
				
				
				//To establish a Connection we need 3 credentials:
				//url (endpoint), username, password
//				String endpoint = "ryan2007sdet.chxttewop8dc.us-east-2.rds.amazonaws.com";
//				String url ="jdbc:oracle:thin:@" + endpoint + ":1521:ORCL";
//				String username = "ryan";
//				String password = "password";
				
				Properties props = new Properties();
				//Will work for this example, 
				//but not later when our application runs on a server
//				FileInputStream input = new FileInputStream("src/main/resources/connection.properties");
				//This solution will work all the way through your dev time.
				FileInputStream input = new FileInputStream(JDBCConnection.class.getClassLoader().getResource("connection.properties").getFile());
				props.load(input);
				
				String url = props.getProperty("url");
				String username = props.getProperty("username");
				String password = props.getProperty("password");
				
				conn = DriverManager.getConnection(url, username, password);
				return conn;
				
				
			} else {
				return conn;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	

}

