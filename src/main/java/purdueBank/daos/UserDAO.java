package purdueBank.daos;

import java.sql.Connection;
import util.JDBCConnection;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import purdueBank.entities.*;

public class UserDAO {
	public static Connection conn = JDBCConnection.getConnection();

	public boolean addUser(User user) {
		// Call ADD_USER
		try {
			String sql = "CALL ADD_USER(?)";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setString(1, user.getName());
			cs.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public List<User> getAllUsers() {

		List<User> users = new ArrayList<User>();

		try {

			String sql = "SELECT * FROM ZUSERS";
			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				User u = new User();
				u.setUser_id(rs.getString("USER_ID"));
				u.setName(rs.getString("NAME"));
				u.setUsername(rs.getString("USERNAME"));
                System.out.println(u.getUser_id()+" "+u.getName()+" "+u.getUsername());
				users.add(u);

			}

			return users;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
}
