package purdueBank.app;

import java.sql.Connection;
import java.util.Scanner;
import util.JDBCConnection;
import purdueBank.services.*;
import purdueBank.entities.*;

public class App {
	private static Scanner scan = new Scanner(System.in);
	private static String[] messages = { "1: Login  2: Register  x: Exit", // 0
			"Enter Username", // 1
			"Enter Password", // 2
			"Login successful", // 3
			"Registration successful", // 4
			"1: View all users  2: Create a user  3: Update a user  4: Delete a user  5: Logout", // 5
			"Not implemented",//6
			"Bye",//7
			"Purdue Bank - Main Menu",//8
			"Superuser Menu",//9
			"User Menu",//10
			"1: View accounts and balances  2: Create account  3: Delete account  4: Make deposit  5: Make withdraw  6: Logout",//11
			"Enter proper name"};

	public static void main(String[] args) {
		//boolean loggedIn = false;
		boolean done = false;
		String choice = "0";
		String in = null;
		String userName = null;
		UserService us = new UserService();
		
		//JDBCConnection  myconn = new JDBCConnection();
		//Connection myconn = JDBCConnection.getConnection();
		//System.out.println(myconn);
		//JDBCConnection.getConnection();
		
		while (!done) {
			switch (choice) {
			case "0": // Main menu
				System.out.println(messages[8]);
				System.out.println(messages[0]);
				in = scan.nextLine();
				switch (in) {
				case "1":
					choice = "1";
					break;
				case "2":
					choice = "2";
					break;
				case "x":
					System.out.println(messages[7]);
					done = true;
					break;
				}
				break;
			case "1": // Login dialog
				System.out.println(messages[1]);
				in = scan.nextLine();
				userName = in;
				System.out.println(messages[2]);
				in = scan.nextLine();
				System.out.println(messages[3]);
				if (userName.equals("superuser")) {
					choice = "3";
				} else {
					choice = "4";
				}

				break;
			case "2": // Register dialog
				System.out.println(messages[1]);
				in = scan.nextLine();
				userName = in;
				System.out.println(messages[2]);
				in = scan.nextLine();
				System.out.println(messages[4]);
				choice = "4";
				break;
			case "3": // Superuser menu
				System.out.println(messages[9]);
				System.out.println(messages[5]);
				in = scan.nextLine();
				switch (in) {
				case "1": //List all users
					us.getAllUsers();
					break;
				case "2":  //Add user
					System.out.println(messages[12]);
					User u = new User();
					u.setName(scan.nextLine());
					us.addUser(u);
					break;
				case "5":
					choice = "0";
					break;
				default:
					System.out.println(messages[6]);
				}
				break;
			case "4": // User menu
				System.out.println(messages[10]);
				System.out.println(messages[11]);
				in = scan.nextLine();
				switch (in) {
				case "6":
					choice = "0";
					break;
				default:
					System.out.println(messages[6]);
				}
				break;
			}
		}

	}

}