package purdueBank.services;
import java.util.List;
import purdueBank.daos.*;
import purdueBank.entities.*;

public class UserService {

	public UserDAO ud = new UserDAO();

	public boolean addUser(User u) {
		return ud.addUser(u);
	}
	public List<User> getAllUsers() {
		return ud.getAllUsers();
	}

}
